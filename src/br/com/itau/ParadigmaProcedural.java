package br.com.itau;

import java.util.Random;

public class ParadigmaProcedural {
    public static void main(String[] args){

        Random random = new Random();
        int soma = 0;
        int random1 = random.nextInt(6) + 1;
        System.out.println("Primeiro numero Randomico: "+ random1);

        System.out.print("Segundo Radom: ");
        for (int i = 0; i < 3; i++ ){
            int num = random.nextInt(6) + 1;
            System.out.print(num + ", ");
            soma = soma + num;
        }
        System.out.print(soma +"\n");

        System.out.println("Terceiro Random");
        for(int x = 0; x < 3; x++){
            soma = 0;
            for (int i = 0; i < 3; i++ ){
                int num = random.nextInt(6) + 1;
                System.out.print(num + ", ");
                soma = soma + num;
            }
            System.out.print(soma + "\n");
        }
    }

}